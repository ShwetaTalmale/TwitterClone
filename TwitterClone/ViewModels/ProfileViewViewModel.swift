//
//  ProfileViewViewModel.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 04/02/23.
//

import Foundation
import Combine
import FirebaseAuth

final class ProfileViewViewModel {
    
    @Published var user: TwitterUser?
    @Published var error: String?
    @Published var tweets: [Tweet] = []
    
    private var subscription: Set<AnyCancellable> = []
    
    func retrieveUser() {
        guard let id = Auth.auth().currentUser?.uid else { return }
        DatabaseManager.shared.collectioUsers(retrieve: id)
            .handleEvents(receiveOutput: { [weak self] user in
                self?.user = user
                self?.fetchTweets()
            })
            .sink { [weak self] completion in
                if case .failure(let error) = completion {
                    self?.error = error.localizedDescription
                }
            } receiveValue: { [weak self] user in
                self?.user = user
            }
            .store(in: &subscription)

    }
    
    func getFormattedDate(with date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM YYYY"
        return dateFormatter.string(from: date)
    }

    func fetchTweets() {
        guard let userId = user?.id else { return}
        DatabaseManager.shared.collectionTweets(retrieveTweets: userId)
            .sink { [weak self] completion in
                if case .failure(let error) = completion {
                    self?.error = error.localizedDescription
                }
            } receiveValue: { [weak self] retrivedTweets in
                self?.tweets = retrivedTweets
            }
            .store(in: &subscription)

    }
}
