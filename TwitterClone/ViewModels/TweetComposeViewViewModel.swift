//
//  TweetComposeViewViewModel.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 04/02/23.
//

import Foundation
import Combine
import FirebaseAuth

final class TweetComposeViewViewModel: ObservableObject {
    private var subscription: Set<AnyCancellable> = []
    
    @Published var isValidToTweet: Bool = false
    @Published var error: String = ""
    @Published var shouldDismissComposer: Bool = false
    var tweetContent: String = ""
    private var user: TwitterUser?
    
    func getUserData() {
        guard let userId = Auth.auth().currentUser?.uid else { return }
        DatabaseManager.shared.collectioUsers(retrieve: userId)
            .sink { [weak self] completion in
                if case .failure(let error) = completion {
                    self?.error = error.localizedDescription
                }
            } receiveValue: { [weak self] tweeterUser in
                self?.user = tweeterUser
            }
            .store(in: &subscription)
    }
    
    func validateToTweet() {
        isValidToTweet = !tweetContent.isEmpty
    }
    
    func dispatchTweet() {
        guard let user = user else { return }
        let tweet = Tweet(author: user,
                          authorID: user.id,
                          tweetContent: tweetContent,
                          likesCount: "0",
                          likers: [],
                          isReply: false,
                          parentReference: nil)
        DatabaseManager.shared.collectionTweet(dispatch: tweet)
            .sink { [weak self] completion in
                if case .failure(let error) = completion {
                    self?.error = error.localizedDescription
                }
            } receiveValue: { [weak self] state in
                self?.shouldDismissComposer = state
            }
            .store(in: &subscription)

    }
}
