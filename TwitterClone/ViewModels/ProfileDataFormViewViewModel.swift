//
//  ProfileDataFormViewViewModel.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 03/02/23.
//

import Foundation
import Combine
import UIKit
import FirebaseStorage
import FirebaseAuth

final class ProfileDataFormViewViewModel: ObservableObject {
    
    private var subscription: Set<AnyCancellable> = []
    
    @Published var displayName: String?
    @Published var userName: String?
    @Published var avatarPath: String?
    @Published var bio: String?
    @Published var imageData: UIImage?
    @Published var isFormValid: Bool = false
    @Published var url: URL?
    @Published var error: String = ""
    @Published var isOnboardingFinished: Bool = false
    
    func validateUserDataForm() {
        guard let displayName = displayName,
              displayName.count > 2,
              let userName = userName,
              userName.count > 2,
              let bio = bio,
              bio.count > 2,
              imageData != nil else {
            isFormValid = false
            return
        }
        isFormValid = true
    }
    
    func uploadAvatar() {
        let randomId = UUID().uuidString
        guard let imageData = imageData?.jpegData(compressionQuality: 0.5) else { return }
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpeg"
        
        StorageManager.shared.uploadProfilePhoto(with: randomId, image: imageData, metaData: metaData)
            .flatMap({ metaData in
                StorageManager.shared.getDownloadURL(for: metaData.path)
            })
        
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    print(error)
                    self?.error = error.localizedDescription
                case .finished:
                    self?.updateUserData()
                }
            } receiveValue: {[weak self] url in
                self?.avatarPath = url.absoluteString
            }
            .store(in: &subscription)

    }
    
    private func updateUserData() {
        guard let userName,
              let displayName,
              let bio,
              let avatarPath,
              let id = Auth.auth().currentUser?.uid else { return }
        
        let updateFields: [String: Any] = [
            "displayName": displayName,
            "userName": userName,
            "bio": bio,
            "avatarPath": avatarPath,
            "isUserOnboarded": true
        ]
        
        DatabaseManager.shared.collectionUsers(updateFields: updateFields, with: id)
            .sink { [weak self] completion in
                if case .failure(let error) = completion {
                    self?.error = error.localizedDescription
                    print(error.localizedDescription)
                }
            } receiveValue: { [weak self] onboardingState in
                self?.isOnboardingFinished = onboardingState
            }
            .store(in: &subscription)

    }
}
