//
//  MainTabBarViewController.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 01/02/23.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        configureTabBarControllers()
    }

    private func configureTabBarControllers() { 
        let homeVC = UINavigationController(rootViewController: HomeViewController())
        homeVC.tabBarItem.image = UIImage(systemName: "house")
        homeVC.tabBarItem.selectedImage = UIImage(systemName: "house.fill")
        
        let searchVC = UINavigationController(rootViewController: SearchViewController())
        searchVC.tabBarItem.image = UIImage(systemName: "magnifyingglass")
        searchVC.tabBarItem.selectedImage = UIImage(systemName: "magnifyingglass.circle.fill")
        
        let notificationVC = UINavigationController(rootViewController: NotificationsViewController())
        notificationVC.tabBarItem.image = UIImage(systemName: "bell")
        notificationVC.tabBarItem.selectedImage = UIImage(systemName: "bell.fill")
        
        let messageVC = UINavigationController(rootViewController: DirectMessagesViewController())
        messageVC.tabBarItem.image = UIImage(systemName: "envelope")
        messageVC.tabBarItem.selectedImage = UIImage(systemName: "envelope.fill")
        
        setViewControllers([homeVC, searchVC, notificationVC, messageVC], animated: true)
    }

}
