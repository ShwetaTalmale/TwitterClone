//
//  HomeViewController.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 01/02/23.
//

import UIKit
import FirebaseAuth
import Combine
import SkeletonView

class HomeViewController: UIViewController {

    // MARK: - Variables declaration

    private let viewModel = HomeViewViewModel()
    private var subscription: Set<AnyCancellable> = []

    private let timelineTableView: UITableView = {
        let tableView = UITableView()
        tableView.register(TweetTableViewCell.self, forCellReuseIdentifier: TweetTableViewCell.identifier)
        return tableView
    }()

    private lazy var composeTweetButton: UIButton = {
        let button = UIButton(type: .system, primaryAction: UIAction { [weak self] _ in
            self?.navigateToTweetCompose()
        })
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .tweeterColor
        button.tintColor = .white
        let plusSign = UIImage(systemName: "plus",
                               withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .bold))
        button.setImage(plusSign, for: .normal)
        button.layer.cornerRadius = 30
        button.clipsToBounds = true
        return button
    }()

    // MARK: - Life cycle methods

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(timelineTableView)
        view.addSubview(composeTweetButton)

        timelineTableView.estimatedRowHeight = 150
        timelineTableView.rowHeight = 150

        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: { [weak self] in
            self?.timelineTableView.stopSkeletonAnimation()
            self?.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
            self?.timelineTableView.reloadData()
        })

        timelineTableView.delegate = self
        timelineTableView.dataSource = self
        configureNavBar()
        bindViews()
        configureConstraints()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        timelineTableView.isSkeletonable = true
        timelineTableView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .gray),
                                                       animation: nil,
                                                       transition: .crossDissolve(0.25))
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        timelineTableView.frame = view.frame
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
        handleAuthentication()
        viewModel.retrieveUser()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: { [weak self] in
            self?.timelineTableView.stopSkeletonAnimation()
            self?.view.hideSkeleton(reloadDataAfter: true, transition: .crossDissolve(0.25))
            self?.timelineTableView.reloadData()
        })
    }

    private func navigateToTweetCompose() {
        let composeTweetVC = UINavigationController(rootViewController: TweetComposeViewController())
        composeTweetVC.modalPresentationStyle = .fullScreen
        present(composeTweetVC, animated: true)
    }

    private func handleAuthentication() {
        if Auth.auth().currentUser == nil {
            let onboardingVC = UINavigationController(rootViewController: OnboardingViewController())
            onboardingVC.modalPresentationStyle = .fullScreen
            present(onboardingVC, animated: true)
        }
    }

    private func completeUserOnboarding() {
        let dataFormVC = ProfileDataFormViewController()
        present(dataFormVC, animated: true)
    }

    private func bindViews() {
        viewModel.$user.sink { [weak self] user in
            guard let user = user else { return }
            if !user.isUserOnboarded {
                self?.completeUserOnboarding()
            }
        }
        .store(in: &subscription) 

        viewModel.$tweets.sink { [weak self] _ in
            DispatchQueue.main.async {
                self?.timelineTableView.reloadData()
            }
        }
        .store(in: &subscription)

    }

    private func configureConstraints() {
        NSLayoutConstraint.activate([
            composeTweetButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25),
            composeTweetButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -120),
            composeTweetButton.widthAnchor.constraint(equalToConstant: 60),
            composeTweetButton.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    private func configureNavBar() {
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        logoImageView.contentMode = .scaleAspectFill
        logoImageView.image = UIImage(named: "twitterLogo")
        
        let middleView = UIView(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        middleView.addSubview(logoImageView)
        navigationItem.titleView = middleView
        
        let profileLogo = UIImage(systemName: "person")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: profileLogo,
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(didTapProfile))
        
        let signOutButton = UIImage(systemName: "rectangle.portrait.and.arrow.right")
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: signOutButton,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(didTapSignOut))
    }
    
    @objc private func didTapSignOut() {
        try? Auth.auth().signOut()
        handleAuthentication()
    }
    
    @objc private func didTapProfile() {
        let profileVC = ProfileViewController()
        navigationController?.pushViewController(profileVC, animated: true)
    }

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension HomeViewController: UITableViewDelegate, SkeletonTableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tweets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TweetTableViewCell.identifier,
                                                       for: indexPath) as? TweetTableViewCell else {
            return UITableViewCell()
        }
        cell.configureTweets(with: viewModel.tweets[indexPath.row])
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return TweetTableViewCell.identifier
    }
}

// MARK: - TweetTableViewCellDelegate
extension HomeViewController: TweetTableViewCellDelegate {
    func tweetTableViewCellDidTapReply() {
        print("Reply")
    }
    
    func tweetTableViewCellDidTapRetweet() {
        print("Retweet")
    }
    
    func tweetTableViewCellDidTapLike() {
        print("Like")
    }
    
    func tweetTableViewCellDidTapShare() {
        print("Share")
    }
}
