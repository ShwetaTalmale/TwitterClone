//
//  SearchViewController.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 01/02/23.
//

import UIKit

class SearchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = "Search"
    }

}
