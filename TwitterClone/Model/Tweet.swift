//
//  Tweet.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 04/02/23.
//

import Foundation

struct Tweet: Codable, Identifiable {
    var id = UUID().uuidString
    let author: TwitterUser
    let authorID: String
    let tweetContent: String
    var likesCount: String
    var likers: [String]
    let isReply: Bool
    let parentReference: String?
}
