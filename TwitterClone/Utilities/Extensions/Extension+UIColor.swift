//
//  Extension+UIColor.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 04/02/23.
//

import UIKit

extension UIColor {
    static let tweeterColor = UIColor(red: 29/255, green: 161/255, blue: 242/255, alpha: 1)
}
