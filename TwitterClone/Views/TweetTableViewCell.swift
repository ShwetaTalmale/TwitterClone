//
//  TweetTableViewCell.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 01/02/23.
//

import UIKit

protocol TweetTableViewCellDelegate: AnyObject {
    func tweetTableViewCellDidTapReply()
    func tweetTableViewCellDidTapRetweet()
    func tweetTableViewCellDidTapLike()
    func tweetTableViewCellDidTapShare()
}

class TweetTableViewCell: UITableViewCell {

   static let identifier = "TweetTableViewCell"
    
    weak var delegate: TweetTableViewCellDelegate?

    private var labelsStackView: UIStackView = {
       let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.distribution = .equalSpacing
        stack.axis = .horizontal
        stack.alignment = .center
        stack.isSkeletonable = true
        return stack
    }()

    private var tweetLabelsStackView: UIStackView = {
       let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.distribution = .equalSpacing
        stack.axis = .horizontal
        stack.alignment = .center
        stack.isSkeletonable = true
        return stack
    }()

    private var buttonStackView: UIStackView = {
       let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.distribution = .equalSpacing
        stack.axis = .horizontal
        stack.alignment = .center
        stack.isSkeletonable = true
        return stack
    }()
    
    private let avatarImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 25
        imageView.layer.masksToBounds = true
        imageView.clipsToBounds = true
        imageView.isSkeletonable = true
        return imageView
    }()
    
    private let displayNameLabel: UILabel = {
       let label = UILabel()
        label.font = .systemFont(ofSize: 18, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        return label
    }()
    
    private let userNameLabel: UILabel = {
       let label = UILabel()
        label.textColor = .secondaryLabel
        label.font = .systemFont(ofSize: 16, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        return label
    }()
    
    private let tweetTextContentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    private let replyButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "bubble.left"), for: .normal)
        button.tintColor = .systemGray2
        return button
    }()
    
    private let retweetButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "arrow.2.squarepath"), for: .normal)
        button.tintColor = .systemGray2
        return button
    }()
    
    private let likeButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "heart"), for: .normal)
        button.tintColor = .systemGray2
        return button
    }()
    
    private let shareButton: UIButton = {
       let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "square.and.arrow.up"), for: .normal)
        button.tintColor = .systemGray2
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        isSkeletonable = true
        contentView.addSubview(labelsStackView)
        labelsStackView.addArrangedSubview(displayNameLabel)
        labelsStackView.addArrangedSubview(userNameLabel)
        contentView.addSubview(tweetLabelsStackView)
        tweetLabelsStackView.addArrangedSubview(tweetTextContentLabel)
        contentView.addSubview(buttonStackView)
        buttonStackView.addArrangedSubview(replyButton)
        buttonStackView.addArrangedSubview(retweetButton)
        buttonStackView.addArrangedSubview(likeButton)
        buttonStackView.addArrangedSubview(shareButton)
        contentView.addSubview(avatarImageView)
        configureConstraints()
        configureButtons()
    }
    
    @objc private func didTapReply() {
        delegate?.tweetTableViewCellDidTapReply()
    }
    
    @objc private func didTapRetweet() {
        delegate?.tweetTableViewCellDidTapRetweet()
    }
    
    @objc private func didTapLike() {
        delegate?.tweetTableViewCellDidTapLike()
    }
    
    @objc private func didTapShare() {
        delegate?.tweetTableViewCellDidTapShare()
    }
    
    func configureTweets(with model: Tweet) {
        displayNameLabel.text = model.author.displayName
        userNameLabel.text = "@\(model.author.userName)"
        tweetTextContentLabel.text = model.tweetContent
        avatarImageView.sd_setImage(with: URL(string: model.author.avatarPath))
    }
    
    private func configureButtons() {
        replyButton.addTarget(self, action: #selector(didTapReply), for: .touchUpInside)
        retweetButton.addTarget(self, action: #selector(didTapRetweet), for: .touchUpInside)
        likeButton.addTarget(self, action: #selector(didTapLike), for: .touchUpInside)
        shareButton.addTarget(self, action: #selector(didTapShare), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureConstraints() {
        NSLayoutConstraint.activate([
            avatarImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            avatarImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 14),
            avatarImageView.heightAnchor.constraint(equalToConstant: 50),
            avatarImageView.widthAnchor.constraint(equalToConstant: 50),

            labelsStackView.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 20),
            labelsStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            labelsStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),

            userNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            userNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),
            userNameLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 120),

            displayNameLabel.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 20),
            displayNameLabel.trailingAnchor.constraint(equalTo: userNameLabel.leadingAnchor, constant: -10),
            displayNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20),

            tweetLabelsStackView.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 20),
            tweetLabelsStackView.topAnchor.constraint(equalTo: labelsStackView.bottomAnchor, constant: 10),
            tweetLabelsStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            
            tweetTextContentLabel.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 20),
            tweetTextContentLabel.topAnchor.constraint(equalTo: labelsStackView.bottomAnchor, constant: 10),
            tweetTextContentLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),

            buttonStackView.leadingAnchor.constraint(equalTo: avatarImageView.trailingAnchor, constant: 20),
            buttonStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            buttonStackView.topAnchor.constraint(equalTo: tweetTextContentLabel.bottomAnchor, constant: 15),
            
            replyButton.leadingAnchor.constraint(equalTo: tweetTextContentLabel.leadingAnchor),
            replyButton.topAnchor.constraint(equalTo: tweetTextContentLabel.bottomAnchor, constant: 10),
            replyButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15),
            
            retweetButton.leadingAnchor.constraint(equalTo: replyButton.trailingAnchor, constant: 60),
            retweetButton.centerYAnchor.constraint(equalTo: replyButton.centerYAnchor),
            
            likeButton.leadingAnchor.constraint(equalTo: retweetButton.trailingAnchor, constant: 60),
            likeButton.centerYAnchor.constraint(equalTo: replyButton.centerYAnchor),
            
            shareButton.leadingAnchor.constraint(equalTo: likeButton.trailingAnchor, constant: 60),
            shareButton.centerYAnchor.constraint(equalTo: replyButton.centerYAnchor),

            labelsStackView.heightAnchor.constraint(equalToConstant: 20),
            tweetLabelsStackView.heightAnchor.constraint(greaterThanOrEqualToConstant: 20),
            buttonStackView.heightAnchor.constraint(equalToConstant: 20)
        ])
    }
    
}
