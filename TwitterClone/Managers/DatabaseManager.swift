//
//  DatabaseManager.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 03/02/23.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreCombineSwift
import Combine

class DatabaseManager {
    static let shared = DatabaseManager()
    
    let database = Firestore.firestore()
    let userPath = "users"
    let tweetPath = "tweets"
    
    func collectionUsers(add user: User) -> AnyPublisher<Bool, Error> {
        let twitterUser = TwitterUser(from: user)
        return database.collection(userPath).document(twitterUser.id).setData(from: twitterUser)
            .map { _ in
                return true
            }
            .eraseToAnyPublisher()
    }
    
    func collectioUsers(retrieve id: String) -> AnyPublisher<TwitterUser, Error> {
        return database.collection(userPath).document(id).getDocument()
            .tryMap {
                try $0.data(as: TwitterUser.self)
            }
            .eraseToAnyPublisher()
    }
    
    func collectionUsers(updateFields: [String: Any], with id: String) -> AnyPublisher<Bool, Error> {
        database.collection(userPath).document(id).updateData(updateFields)
            .map { _ in true}
            .eraseToAnyPublisher()
        
    }
    
    func collectionTweet(dispatch tweet: Tweet) -> AnyPublisher<Bool, Error> {
        database.collection(tweetPath).document(tweet.id).setData(from: tweet)
            .map { _ in true}
            .eraseToAnyPublisher()
    }
    
    func collectionTweets(retrieveTweets forUserId: String) -> AnyPublisher<[Tweet], Error> {
        database.collection(tweetPath).whereField("authorID", isEqualTo: forUserId)
            .getDocuments()
            .tryMap(\.documents)
            .tryMap { snapshots in
                try snapshots.map({
                    try $0.data(as: Tweet.self)
                })
            }
            .eraseToAnyPublisher()
    }

    func collectionAllTweets() -> AnyPublisher<[Tweet], Error> {
        database.collection(tweetPath)
            .getDocuments()
            .tryMap(\.documents)
            .tryMap { snapshots in
                try snapshots.map({
                    try $0.data(as: Tweet.self)
                })
            }
            .eraseToAnyPublisher()
    }
}
