//
//  StorageManager.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 03/02/23.
//

import Foundation
import Combine
import FirebaseStorage
import FirebaseStorageCombineSwift

enum FireStorageError: Error {
    case invalidImageID
}

final class StorageManager {
    static let shared = StorageManager()
    
    let storage = Storage.storage()
    
    func uploadProfilePhoto(with randonId: String,
                            image: Data,
                            metaData: StorageMetadata) -> AnyPublisher<StorageMetadata, Error> {
        return storage
            .reference()
            .child("images/\(randonId).jpg")
            .putData(image, metadata: metaData)
            .eraseToAnyPublisher()
    }
    
    func getDownloadURL(for id: String?) -> AnyPublisher<URL, Error> {
        guard let id = id else {
            return Fail(error: FireStorageError.invalidImageID)
                .eraseToAnyPublisher()
        }
        return storage
            .reference(withPath: id)
            .downloadURL()
            .eraseToAnyPublisher()
    }
}
