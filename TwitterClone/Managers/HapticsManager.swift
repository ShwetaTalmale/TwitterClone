//
//  HapticsManager.swift
//  TwitterClone
//
//  Created by Shweta Talmale on 21/02/23.
//

import UIKit

final class HapticsManager {
    static let shared = HapticsManager()

    private init() {}

    func vibrate(for type: UINotificationFeedbackGenerator.FeedbackType) {
        let notificationGenerator = UINotificationFeedbackGenerator()
        notificationGenerator.prepare()
        notificationGenerator.notificationOccurred(type)
    }
}
